Clear-Host

## Création des utlisateurs
$users = @(
	@{ firstName = "Angelina"; lastName = "Jolie" },
    @{ firstName = "Brad"; lastName = "Pitt" },
    @{ firstName = "Nicolas"; lastName = "Hollande" }
)



$password = "Azerty123@" | ConvertTo-SecureString -AsPlainText -Force
$domain = "dawan.fr"

foreach ($user in $users) {
    $name = $user.firstName + " " + $user.lastName.ToUpper()
	$displayName = $user.firstName + " " + $user.lastName.ToUpper()
	$givenName = $user.firstName
	$surName = $user.lastName
	$samAccountName = ($user.firstName + '.' + $user.lastName).ToLower()
	$userPrincipalName = $samAccountName + "@" + $domain
	$emailAddress = $userPrincipalName
	$path = "OU=Users,OU=Angleterre,DC=dawan,DC=local"


	"Adding user " + $name
	New-ADUser -Name $name `
		-DisplayName $displayName `
		-GivenName $givenName `
		-Surname $surName `
		-SamAccountName $samAccountName `
		-UserPrincipalName $userPrincipalName `
		-EmailAddress $emailAddress `
		-Path $path `
		-AccountPassword $password `
		-PasswordNeverExpires $false `
		-ChangePasswordAtLogon $true `
		-Enabled $true
}