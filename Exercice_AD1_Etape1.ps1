
## PARAMÈTRES DE BASE
## Renommer la machine (nécessite un restart, voir ligne 14)
Rename-Computer -NewName "TEST-DC01"
## Récupérer l'index via Get-NetAdapter
## Note: Des commandes complexes existent pour filter et affecter l'index à une variable
Get-NetAdapter

## Pile IPv4
New-NetIPAddress -InterfaceIndex 5 -IPAddress 192.168.200.11 -PrefixLength 24 -DefaultGateway 192.168.200.1
Set-DnsClientServerAddress -InterfaceIndex 5 -ServerAddresses ("192.168.200.11","127.0.0.1")

## Reboot
Restart-Computer


## Ajout du rôle ADDS
Add-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools -IncludeAllSubFeature

## Ajout du rôle DNS
Add-WindowsFeature -Name DNS -IncludeManagementTools -IncludeAllSubFeature

## Ajout des RSAT
Add-WindowsFeature -Name RSAT-AD-Tools -IncludeManagementTools -IncludeAllSubFeature


## INSTALLATION DE L'AD
## Paramètres de l'AD
$CreateDnsDelegation = $false
$DomainName = "test-formation.lan"
$NetbiosName = "TEST-FORMATION"
$NTDSPath = "C:\Windows\NTDS"
$LogPath = "C:\Windows\NTDS"
$SysvolPath = "C:\Windows\SYSVOL"
$DomainMode = "Default"
$InstallDNS = $true
$ForestMode = "Default"
$NoRebootOnCompletion = $true
$SafeModeClearPassword = "Entrez_votre_MDP_ici"
$SafeModeAdministratorPassword = ConvertTo-SecureString $SafeModeClearPassword -AsPlaintext -Force

## Installation de l'AD, création de la forêt
Install-ADDSForest `
-CreateDnsDelegation:$CreateDnsDelegation `
-DomainName $DomainName `
-DatabasePath $NTDSPath `
-DomainMode $DomainMode `
-DomainNetbiosName $NetbiosName `
-ForestMode $ForestMode `
-InstallDNS:$InstallDNS `
-LogPath $LogPath `
-NoRebootOnCompletion:$NoRebootOnCompletion `
-SysvolPath $SysvolPath `
-SafeModeAdministratorPassword $SafeModeAdministratorPassword `
-Force:$true

## Reboot
Restart-Computer

## INSTALLATION DU SERVEUR DHCP
# Ajout du rôle serveur DHCP
Install-WindowsFeature DHCP -IncludeManagementTools

# Créer les groupes de sécurité administrateurs et utilisateurs DHCP
netsh dhcp add securitygroups
Restart-Service dhcpserver

# Supprimer le message de configuration nécessaire dans le gestionnaire de serveurs
Set-ItemProperty –Path registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\ServerManager\Roles\12 –Name ConfigurationState –Value 2

# Autoriser le DHCP à écrire dans l'AD
Add-DhcpServerInDC -DnsName "test-formation.lan"

# Création de l'étendue "par défaut"
Add-DhcpServerv4Scope -Name "VLAN-DATA" -StartRange 192.168.200.1 -EndRange 192.168.200.254 -SubnetMask 255.255.255.0 -State Active

# Étendues exclues
Add-DhcpServerv4ExclusionRange -ScopeID 192.168.200.0 -StartRange 192.168.200.1 -EndRange 192.168.200.100
Add-DhcpServerv4ExclusionRange -ScopeID 192.168.200.240 -StartRange 192.168.200.1 -EndRange 192.168.200.254
# Exclure l'adresse du serveur lui-même
Add-DhcpServerv4ExclusionRange -ScopeID 192.168.200.0 -StartRange 192.168.200.11 -EndRange 192.168.200.11

# Paramètres de domaine, serveur DNS et passerelle
## Router
Set-DhcpServerv4OptionValue -ScopeID 192.168.200.0 -OptionID 3 -Value 192.168.200.1
## Nom de Domaine et serveurs de nom de domaine
Set-DhcpServerv4OptionValue -ScopeID 192.168.200.0 -DnsDomain "test-formation.lan" -DnsServer 192.168.200.11

## DNS: Ajout des resolveurs externes (Orange)
$forwarders = @('80.10.246.129', '80.10.246.2')
foreach ($address in $forwarders) {
	Add-DnsServerForwarder -IPAddress $address -PassThru
}
