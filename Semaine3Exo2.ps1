<#
    Créer une fonction "Encadre-titre"
    qui affiche les choses de cette manière:

    ***************
    ** Mon titre **
    ***************

    ---------------
    -- Mon titre --
    ---------------

    ###############
    ## Mon titre ##
    ###############
#>


function Encadre-titre( $titre, $char="*" ) {
    $frame = "$char" * ($titre.Length + 6)
    Write-Host $frame
    Write-Host ($char*2 + " $titre " + $char*2)
    Write-Host $frame
}

# Exemples
Encadre-titre -titre "Hello !"
Encadre-titre -titre "Hello World !" -char "-"
Encadre-titre -titre "Hello PowerShell !" -char "#"

## Cette syntaxe provoque une erreur :/
# Encadre-titre ( $titre="Hello PowerShell !", $char="#")

## Celle-ci me renvoie un résultat alakon
# Encadre-titre ( "Hello PowerShell !", "#")

