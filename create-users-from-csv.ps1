##
## add-users.ps1 - Add users from CSV file
##

$users = Import-Csv -Path "./users.csv" -Delimiter ';' -Encoding UTF8
$Password = ConvertTo-SecureString 'a12345!' -Force -AsPlainText
$dn = "DC=tuxtelecom,DC=local"
$domain = "tuxtelecom.net"

## Create Users
foreach($user in $users) {

	## TODO: Create function FormatString()
	$firstName = ($user.Prenom.Substring(0,1).ToUpper() + $user.Prenom.Substring(1))
	$lastName = $user.Nom.ToUpper()
	$displayName = $firstName + " " + $lastName
	$city = ($user.Ville.Substring(0,1).ToUpper() + $user.Ville.Substring(1))
	$department = $user.Service
	$samAccountName = ($firstName.Substring(0,1).ToLower() +'.'+ $lastName.ToLower() )
	$userPrincipalName = $samAccountName + "@" + $domain
	$emailAddress = $userPrincipalName

	## Check target object
	## If the target OU does not exists, skip to protect the LDAP structure
	$Path = "OU=$department,OU=Utilisateurs,OU=$city,OU=DAWAN,$dn"
	if ( !(Get-ADOrganizationalUnit -Filter {distinguishedName -eq $Path}) ) {
		Write-Host "OU $Path does not exists, skipping user $samAccountName"
		continue
	}

	## Check user object
	$UserPath = "CN=$firstName $lastName,OU=$department,OU=Utilisateurs,OU=$city,OU=DAWAN,$dn"
	if ( !(Get-ADUser -Filter {distinguishedName -eq $UserPath}) ) {
		Write-Host "Create User: $samAccountName"
		New-ADUser -Name $displayName `
			-DisplayName $displayName `
			-GivenName $firstName `
			-Surname $lastName `
			-SamAccountName $samAccountName `
			-UserPrincipalName $userPrincipalName `
			-EmailAddress $emailAddress `
			-Path $Path `
			-AccountPassword $Password `
			-PasswordNeverExpires $false `
			-ChangePasswordAtLogon $true `
			-Enabled $true
	} else {
		Write-Host "User $samAccountName exists, skipping..."
	}
}


## Buitd group names
## TODO: improve
## get cities
$tmp = @()
foreach ($user in $users) {
	$tmp += $user.Ville
}
$cities = $tmp | Sort-Object | Get-Unique

## get departments
$tmp = @()
foreach ($user in $users) {
	$tmp += $user.Service
}
$departments = $tmp | Sort-Object | Get-Unique
Remove-Variable tmp

## generate names
foreach ($city in $cities) {
	foreach ($department in $departments) {
		$name = ("GG_$city" + "_" + $department)
		$cityPath = ("OU=$city" + ",OU=DAWAN," + $dn)
		$Path = ("OU=Groupes," + $cityPath)
		$objectDN = ("CN=" + $name + ',' + $Path)

		if ( !( Get-ADOrganizationalUnit -Filter {distinguishedName -eq $Path} ) ) {
			Write-Host "OU $Path does not exists, skipping group $name"
			continue
		}

		if ( !( Get-ADGroup -Filter {distinguishedName -eq $objectDN} ) ) {
			New-ADGroup -Name $name -GroupScope Global -Path $Path
		} else {
			Write-Host "Group $name already exists, skipping"
		}
	}
}

