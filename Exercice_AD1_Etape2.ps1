
## PARAMÈTRES DE BASE
## Renommer la machine (nécessite un restart, voir ligne 14)
Rename-Computer -NewName "TEST-APPLIS"
## Récupérer l'index via Get-NetAdapter
## Note: Des commandes complexes existent pour filter et affecter l'index à une variable
Get-NetAdapter

## Amélioration
$ifName = "Ethernet"
$ifIndex = (Get-NetAdapter | Where-Object Name -eq $ifName).ifIndex  

## Pile IPv4
New-NetIPAddress -InterfaceIndex $ifIndex -IPAddress 192.168.200.21 -PrefixLength 24 -DefaultGateway 192.168.200.1
Set-DnsClientServerAddress -InterfaceIndex $ifIndex -ServerAddresses 192.168.200.11

## Reboot
Restart-Computer

## Jonction au domaine
Add-Computer -DomainName test-formation.lan -DomainCredential administrateur@test-formation.lan

## Reboot
Restart-Computer

## Installer le rôle serveur de fichiers
Add-WindowsFeature -Name FS-FileServer -IncludeManagementTools

## Installer le rôle serveur d'impression
Add-WindowsFeature -Name Print-Server -IncludeManagementTools

