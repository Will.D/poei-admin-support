## Configuration du client NTP sur le DC

# Arrêter le service de temps
net stop w32time

# Configuration NTP
w32tm /config /syncfromflags:manual /manualpeerlist:"0.fr.pool.ntp.org"
w32tm /config /reliable:yes

# Démarrer le service de temps
net start w32time

# Forcer la mise à jour de l'orloge
w32tm /resync /force

# Vérification
w32tm /quey /status
