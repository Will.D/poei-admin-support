
## Usage: DC-CreateUsers.ps1 -CsvFile <csv file path> [-Domain <FQDN>]
## Example: .\DC-CreateUsers.ps1 -CsvFile .\Users.csv
## Example: .\DC-CreateUsers.ps1 -CsvFile .\Users.csv -Domain toto.com

param(
	[Parameter(mandatory)] [String]$CsvFile,
	[Parameter()] [String]$Domain = (Get-ADDomain -Current LocalComputer).DNSRoot,
	[Parameter()] [String]$RootOU = 'ou=_' + ($Domain.split('.')[0]).ToUpper()
)


function Remove-StringSpecialCharacters {
   param ([string]$String)

   [Text.Encoding]::ASCII.GetString([Text.Encoding]::GetEncoding("Cyrillic").GetBytes($String)) `
      -replace '-', '' `
      -replace ' ', '' `
      -replace '/', '' `
      -replace '\*', '' `
      -replace "'", "" 
}


$Users = Import-Csv -Path $CsvFile -Delimiter ';' -Encoding UTF8
$BasePath = ($RootOU + ',dc=' + $Domain.split('.')[0] + ',dc=' + $Domain.split('.')[1])
## Base DN

foreach ($User in $Users) {
	$User.city = (Remove-StringSpecialCharacters -String $User.city).ToUpper()
	$GivenName = (Get-Culture).TextInfo.ToTitleCase($User.first_name.ToLower())
	$SurName = (Get-Culture).TextInfo.ToTitleCase($User.last_name.ToLower())
	$SamAccountName = (Remove-StringSpecialCharacters -String $GivenName).ToLower() + '.' + (Remove-StringSpecialCharacters -String $SurName).ToLower()

	if ( (Get-ADUser -Filter { SamAccountName -eq $SamAccountName }) ) {
		Write-Host "User $SamAccountName already exists: Skipping"
	}
	else {
		
		$Name = (Remove-StringSpecialCharacters -String $GivenName) + " " + (Remove-StringSpecialCharacters -String $SurName)
		$DisplayName = $GivenName + " " + $SurName
		$UserPrincipalName = $SamAccountName + '@' + $Domain
		$Path = 'OU=Utilisateurs,' + 'OU=' + $User.city + ',' + $BasePath
		
		$SecretPassword = $User.clear_password | ConvertTo-SecureString -AsPlainText -Force
		
		Write-Host "Creating $SamAccountName under $Path"
		# Write-Host $GivenName
		# Write-Host $SurName
		# Write-Host $Name
		# Write-Host $DisplayName
		# Write-Host $SamAccountName
		# Write-Host $UserPrincipalName
		# Write-Host $Path
		# Write-Host ''
	
		New-ADUser -Name "$Name" `
			-DisplayName "$DisplayName" `
			-GivenName "$GivenName" `
			-Surname "$SurName" `
			-SamAccountName "$SamAccountName" `
			-UserPrincipalName "$UserPrincipalName" `
			-Path "$Path" `
			-AccountPassword $SecretPassword `
			-PasswordNeverExpires $false `
			-ChangePasswordAtLogon $true `
			-Enabled $true
	}
}