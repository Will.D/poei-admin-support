
## Usage: $$.ps1 -CsvFile <csv file path> [-Domain <FQDN>]
## Example: .\$$.ps1 -CsvFile .\Users.csv
## Example: .\$$.ps1 -CsvFile .\Users.csv -Domain toto.com

param(
	[Parameter(mandatory)] [String]$CsvFile,
	[Parameter()] [String]$Domain = (Get-ADDomain -Current LocalComputer).DNSRoot,
	[Parameter()] [String]$RootOU = 'ou=_' + ($Domain.split('.')[0]).ToUpper()
)

function Remove-StringSpecialCharacters {
	param ([string]$String)
 
	[Text.Encoding]::ASCII.GetString([Text.Encoding]::GetEncoding("Cyrillic").GetBytes($String)) `
	   -replace '-', '' `
	   -replace ' ', '' `
	   -replace '/', '' `
	   -replace '\*', '' `
	   -replace "'", "" 
 }

$Users = Import-Csv -Path $CsvFile -Delimiter ';' -Encoding UTF8
$BasePath = ($RootOU + ',dc=' + $Domain.split('.')[0] + ',dc=' + $Domain.split('.')[1])
## Base DN

foreach ($User in $Users) {
	$User.city = (Remove-StringSpecialCharacters -String $User.city).ToUpper()
	$User.department = Remove-StringSpecialCharacters $User.department
	$User.department = (Get-Culture).TextInfo.ToTitleCase($User.department.ToLower())
	$GivenName = (Get-Culture).TextInfo.ToTitleCase($User.first_name.ToLower())
	$SurName = (Get-Culture).TextInfo.ToTitleCase($User.last_name.ToLower())
	$UserSamAccountName = (Remove-StringSpecialCharacters -String $GivenName).ToLower() + '.' + (Remove-StringSpecialCharacters -String $SurName).ToLower()

	## TODO check user
	## TODO check membership

	## Get user city and department and build group SamAccountName
	$GroupSamAccountName = 'GG_' + $User.city + '_' + $User.department
	$Path = 'cn=' + $GroupSamAccountName + ',ou=Groupes,ou=' + $User.city + ',' + $BasePath

	## Add user in group
	Write-Host "Adding user $UserSamAccountName in $Path"
	Add-ADPrincipalGroupMembership `
		-Identity $UserSamAccountName `
		-MemberOf $Path
}