
## Optionnel: Installer les Guest Additions
## ça n'améliorera pas l'affichage mais ça peut servir
## Retrouver le lecteur
Get-PSDrive -PSProvider FileSystem
## Installer les Additions
D:\VBoxWindowsAdditions-amd64.exe



## Renommer la machine et redémarrer
Rename-Computer -NewName "DC1"
Restart-Computer


## Configurer le réseau
# Récupérer l'index de l'interface active
# (Le filtre peut être amélioré. J'utilise ici l'adresse MAC)
$MACAddress = "08-00-27-DE-FC-D7"
$ifIndex = (Get-NetAdapter | Where-Object -Property MacAddress -eq $MACAddress).ifIndex

$IPAddress = '192.168.100.250'
$Prefix = '24'
$Gateway = '192.168.100.254'
$DNSServerAddress = @('127.0.0.1', '8.8.8.8')

# Adresse, masque, passerelle
New-NetIPAddress `
    -InterfaceIndex $ifIndex `
    -IPAddress $IPAddress `
    -PrefixLength $Prefix `
    -DefaultGateway $Gateway

# Serveur(s) DNS
Set-DnsClientServerAddress `
    -InterfaceIndex $ifIndex `
    -ServerAddresses $DNSServerAddress


## Configurer le service NTP
# Arrêter le service de temps
net stop w32time

# Configurer le NTP
w32tm /config /syncfromflags:manual /manualpeerlist:"fr.pool.ntp.org"
w32tm /config /reliable:yes

# Démarrer le service de temps
net start w32time

# Forcer la syncro (si le réseau et le DNS sont en place)
w32tm /resync /force

# Verification
w32tm /query /status



## Changer la politique d'exécution des scripts
Set-ExecutionPolicy Unrestricted



## Installer OpenSSH-Server (sshd)
# Récupérer la version disponible
Get-WindowsCapability -Online | Where-Object Name -like "OpenSSH*"

# Installer OpenSSH-Server
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0

## Démarrer OpenSSH-Server (sshd)
Start-Service sshd

# Démarrage automatique
Set-Service -Name sshd -StartupType 'Automatic'

## Créer une règle de pare-feu autorisant le SSH si elle n'existe pas
if ( !( Get-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' -ErrorAction SilentlyContinue | Select-Object Name,Enabled ) ) {
    New-NetFirewallRule `
        -Name 'OpenSSH-Server-In-TCP' `
        -DisplayName 'OpenSSH Server (sshd)' `
        -Direction Inbound `
        -Protocol TCP `
        -LocalPort 22 `
        -Action Allow `
        -Enabled True
}



