
## Usage: DC-CreateGlobalGroups.ps1 -CsvFile <csv file path> [-Domain <FQDN>]
## Example: .\DC-CreateGlobalGroups.ps1 -CsvFile .\Users.csv
## Example: .\DC-CreateGlobalGroups.ps1 -CsvFile .\Users.csv -Domain toto.com

param(
	[Parameter(mandatory)] [String]$CsvFile,
	[Parameter()] [String]$Domain = (Get-ADDomain -Current LocalComputer).DNSRoot,
	[Parameter()] [String]$RootOU = 'ou=_' + ($Domain.split('.')[0]).ToUpper()
)


function Remove-StringSpecialCharacters {
   param ([string]$String)

   [Text.Encoding]::ASCII.GetString([Text.Encoding]::GetEncoding("Cyrillic").GetBytes($String)) `
      -replace '-', '' `
      -replace ' ', '' `
      -replace '/', '' `
      -replace '\*', '' `
      -replace "'", "" 
}


$Users = Import-Csv -Path $CsvFile -Delimiter ';' -Encoding UTF8
$BasePath = ($RootOU + ',dc=' + $Domain.split('.')[0] + ',dc=' + $Domain.split('.')[1])
## Base DN

$GroupList = @()
foreach ($User in $Users) {
	$User.department = Remove-StringSpecialCharacters $User.department
	$User.department = (Get-Culture).TextInfo.ToTitleCase($User.department.ToLower())

	$User.city = Remove-StringSpecialCharacters $User.city.ToUpper()

	$GroupName = 'GG_' + $User.city + '_' + $User.department
	if (!($GroupName -in $GroupList)) {
		$GroupList += $GroupName
	}
}

foreach ($Group in $GroupList) {
	$City = $Group.split('_')[1]
#	$GroupCN = 'cn=' + $Group + ',ou=Groupes,ou=' + $City + ',' + $BasePath
#	Write-Host $GroupCN
	$GroupPath = 'ou=Groupes,ou=' + $City + ',' + $BasePath
	$SamAccountName = $Group

	if ( (Get-ADGroup -Filter { SamAccountName -eq $SamAccountName }) ) {
		Write-Host "Group $SamAccountName already exists: Skipping"
	}
	else {
		Write-Host "Creating group $SamAccountName under $GroupPath"
		New-ADGroup `
			-GroupScope Global `
			-Name $Group `
			-Path $GroupPath `
			-GroupCategory Security
	}
}

