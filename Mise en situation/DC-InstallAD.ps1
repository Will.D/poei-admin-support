
## Ajout des rôles et fonctionalités
## Ajout du rôle ADDS
Add-WindowsFeature -IncludeManagementTools -IncludeAllSubFeature -Name AD-Domain-Services

## Ajout du rôle DNS
Add-WindowsFeature -IncludeManagementTools -IncludeAllSubFeature -Name DNS

## Ajout des RSAT
Add-WindowsFeature -IncludeManagementTools -IncludeAllSubFeature -Name RSAT-AD-Tools



## Configuration de l'AD
## NOTE: J'ai changé le thème de l'entreprise
$CreateDnsDelegation = $false
$DomainName = "techdef.lan"
$NetbiosName = "TECHDEF"
$NTDSPath = "C:\Windows\NTDS"
$LogPath = "C:\Windows\NTDS"
$SysvolPath = "C:\Windows\SYSVOL"
$DomainMode = "Default"
$InstallDNS = $true
$ForestMode = "Default"
$NoRebootOnCompletion = $true
$SafeModeClearPassword = "Entrez_votre_MDP_ici"
$SafeModeAdministratorPassword = ConvertTo-SecureString $SafeModeClearPassword -AsPlaintext -Force

## Installation de l'AD, création de la forêt
Install-ADDSForest `
	-CreateDnsDelegation:$CreateDnsDelegation `
	-DomainName $DomainName `
	-DatabasePath $NTDSPath `
	-DomainMode $DomainMode `
	-DomainNetbiosName $NetbiosName `
	-ForestMode $ForestMode `
	-InstallDNS:$InstallDNS `
	-LogPath $LogPath `
	-NoRebootOnCompletion:$NoRebootOnCompletion `
	-SysvolPath $SysvolPath `
	-SafeModeAdministratorPassword $SafeModeAdministratorPassword `
	-Force:$true

## Reboot
Restart-Computer

## Après reboot: Ajout des forwarders DNS (exemple avec les DNS Orange)
$forwarders = @('80.10.246.129', '80.10.246.2')
foreach ($address in $forwarders) {
	Add-DnsServerForwarder -IPAddress $address -PassThru
}

