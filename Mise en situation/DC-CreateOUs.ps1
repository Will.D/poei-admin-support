## file:  DC-CreateOUs.ps1

## Usage: DC-CreateOUs.ps1 -Domain <domain.tld> -Cities <city1,city2,city3[...]>
##        La liste des villes n'est pas sensible à la casse
##        Il est préférable de fournir la liste des viller directement, il s'agit d'une liste


## Example: DC-CreateOUs.ps1 -Domain papier.lan -Cities Paris,Bordeaux,Marseille,Nantes,Strasbourg


param(
	[Parameter(mandatory)] [String]$Domain,
	[Parameter(mandatory)] [String[]]$Cities
)


## Cette fonction vérifie l'existence de l'OU $Name
## au sein de l'arborescence $SearchBase
function CheckOU {
	[CmdletBinding()]
	param (
		[String]$SearchBase  = (Get-ADDomain -Current LocalComputer).DistinguishedName,
		[Parameter(mandatory)] [String]$Name
	)


	$request = Get-ADObject -LDAPFilter "(objectClass=OrganizationalUnit)" -SearchBase $SearchBase | Where-Object -Property Name -eq $Name

	if ( $request ) {
		## Object found
		return $True
	}
	## Object not found
	return $False
}




$DomainOU = '_' + $Domain.split('.')[0].ToUpper()
$Path = ('dc=' + $Domain.split('.')[0] + ',dc=' + $Domain.split('.')[1]).ToLower()
$BaseName = $Path


## Création de l'OU Principale
## NOTE: On autorise la suppression
$ProtectedFromAccidentalDeletion = $False
if (!(CheckOU -SearchBase $BaseName -Name $DomainOU)) {
	Write-Host "Creating OU $DomainOU under $BaseName"
	New-ADOrganizationalUnit `
		-Name $DomainOU `
		-ProtectedFromAccidentalDeletion $ProtectedFromAccidentalDeletion `
		-Path $Path
}
else {
	Write-Host "OU=$DomainOU,$BaseName already exists: Skipping"
}


## Création des OUs de villes
## Path: OU=_DOMAIN,dc=DOMAIN,dc=TLD
$Path = 'OU=' + $DomainOU + ',' + $BaseName
foreach ($City in $Cities) {
	$City = $City.ToUpper()
	if ( ! (CheckOU -SearchBase $BaseName -Name $City) ) {
		Write-Host "Creating OU $City under $Path"
		New-ADOrganizationalUnit `
			-Name $City `
			-Path $Path `
			-ProtectedFromAccidentalDeletion $False
	} else {
		Write-Host "OU=$City,$BaseName already exists: Skipping"
	}
}

$OUs = @(
	'Groupes',
	'Utilisateurs',
	'Ordinateurs'
)

## Création des OUs d'objets
foreach ($City in $Cities) {
	$City = $City.ToUpper()
	$Path = 'OU=' + $City + ',OU=' + $DomainOU + ',' + $BaseName
	foreach ($OU in $OUs) {
		if ( ! (CheckOU -SearchBase $Path -Name $OU) ) {
			Write-Host "Creating OU $OU under $Path"
			New-ADOrganizationalUnit `
			-Name $OU `
			-Path $Path `
			-ProtectedFromAccidentalDeletion $False
		} else {
			Write-Host "OU=$OU,$Path already exists: Skipping"
		}
	}

}
