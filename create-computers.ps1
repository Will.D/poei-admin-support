Clear-Host

$max = 100
$percent = 0

foreach ($i in 1..$max) {
	New-ADComputer `
		-Name "Ordi$i" `
		-Path "OU=Computers,OU=Angleterre,DC=dawan,DC=local"
	Write-Host "Ordi$i created" -ForegroundColor Cyan
	$percent = [Math]::Truncate( ($i/$max)*100 )
	Write-Progress -Activity "Object creation" -Status "$percent% complete" -PercentComplete $percent
}
