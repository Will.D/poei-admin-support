# TP
#cd 'C:\PowerShell'
# 1- Créer un répertoire TMP
# 2- Créer 200 fichiers vide de 1.log à 200.log
# 3- Renommer les fichiers 0001.log à 0200.log

$workDir = 'C:\PowerShell\TMP'

## Créer le répertoire TMP s'il n'existe pas
if (! (Test-Path -Path $workDir -PathType Container) ) {
    mkdir $workDir | out-null
}


## Création des fichiers
foreach ( $i in 1..200 ) {
	if (! (Test-Path -Path "$workDir/$i.log" -PathType Leaf) ) {
		New-Item "$workDir/$i.log" | out-null
    }
}

## Renommage des fichiers
$zeroes = ""
foreach ( $file in (Get-ChildItem "$workDir") ) {
    if ( $file.Name -Like "*.log" ) {
        # 0-9.log
        if ( $file.Name.Length -eq 5 ) {
            $zeroes = "000"
        }
        # 10-99.log
        elseif ( $file.Name.Length -eq 6 ) {
            $zeroes = "00"
        }
        #100-999.log
        else {
            $zeroes = "0"
        }
        $newName = $zeroes + $file.Name
        Write-Host $newName # DEBUG
        Rename-Item -Path $file.FullName -NewName $newName | out-null
    }
}
