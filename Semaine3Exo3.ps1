<#
    Créer une Fonction Dire-BjLang
    Dire-BjLang -Prenom Jean -Nom Dupont -Lang 'FR' -Nb 3
#>


function Dire-BjLang($Prenom, $Nom, $Lang='FR', $Nb) {
    switch ($Lang) {
        'EN' { $hello = 'Hello' }
        'ES' { $hello = 'Hola' }
        Default { $hello = 'Bonjour'}
    }

    ## Format Prenom
    if ( ! $Prenom -eq "" ) {
        $fPrenom = $Prenom.Substring(0,1).ToUpper() + $Prenom.Substring(1).ToLower()
    }

    # Format Nom
    if ( ! $Nom -eq "") {
        $fNom = $Nom.ToUpper()
    }

    # Build string
    if ( ! $Prenom -eq "" -and ! $Nom -eq "" ) {
        $FullName = "$fPrenom $fNom"
    } elseif ( ! $Prenom -eq "" ) {
        $FullName = $fPrenom
    } elseif ( ! $Nom -eq "" ) {
        $FullName = $fNom
    }

	# Print
    do {
        Write-Host ("$hello $FullName")
        $i++
    } while ($i -lt $Nb)
}

Dire-BjLang
Dire-BjLang -Prenom "Zit"
Dire-BjLang -Prenom "ziT" -Nom "mactavish"
Dire-BjLang -Nom "MacTavish" -Lang 'ES' -Nb 10
Dire-BjLang -Prenom "Zit" -Nom "MacTavish" -Lang 'EN' -Nb 3
