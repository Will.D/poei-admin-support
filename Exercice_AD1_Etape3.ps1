
## Importation des fonctions AD dans l'environnement
Import-module ActiveDirectory

## Création de l'OU principale à la racine
New-ADOrganizationalUnit -Name "_TEST-FORMATION" -Path "DC=test-formation,DC=lan"

## Création des OUs sous l'OU principale
$rootOU = "OU=_TEST-FORMATION,DC=test-formation,DC=lan"
$OUs = @("Groupes","Ordinateurs","Serveurs","Utilisateurs")

foreach ($OU in $OUs) {
	New-ADOrganizationalUnit -Name $OU -Path $rootOU
}



## Création des utlisateurs
$users = @(
	@{ firstName = "Jake"; lastName = "Directeur"},
	@{ firstName = "Mourad"; lastName = "Comptable"},
	@{ firstName = "Leila"; lastName = "Accueil"},
	@{ firstName = "Pierre"; lastName = "Reprographie"},
	@{ firstName = "Ali"; lastName = "Communication"}
)

## Utilisation d'un mot de passe commun.
## L'idéal serait de générer un mot de passe aléatoire
## + à chaque tour de boucle.
## Mot de passe super robuste :P
$password = "Azerty123@" | ConvertTo-SecureString -AsPlainText -Force
$domain = "test-formation.lan"

foreach ($user in $users) {
	$name = $user.firstName + " " + $user.lastName
	$displayName = $user.firstName + " " + $user.lastName.ToUpper()
	$givenName = $user.firstName
	$surName = $user.lastName
	$samAccountName = ($user.firstName.SubString(0,1) + $user.lastName).ToLower()
	$userPrincipalName = $samAccountName + "@" + $domain
	$emailAddress = $userPrincipalName
	$path = "OU=Utilisateurs,OU=_TEST-FORMATION,DC=TEST-FORMATION,DC=LAN"

	"Adding user " + $name
	New-ADUser -Name $name `
		-DisplayName $displayName `
		-GivenName $givenName `
		-Surname $surName `
		-SamAccountName $samAccountName `
		-UserPrincipalName $userPrincipalName `
		-EmailAddress $emailAddress `
		-Path $path `
		-AccountPassword $password `
		-PasswordNeverExpires $false `
		-ChangePasswordAtLogon $true `
		-Enabled $true
}

## Création des groupes

$path = "OU=Groupes,OU=_TEST-FORMATION,DC=TEST-FORMATION,DC=LAN"

$groups = @(
	@{name = "G_Direction"; scope = "Global"},
	@{name = "G_Accueil"; scope = "Global"},
	@{name = "G_Compta"; scope = "Global"},
	@{name = "G_Repro"; scope = "Global"},
	@{name = "G_Comm"; scope = "Global"},
	@{name = "G_TEST-FORMATION"; scope = "Global"},
	@{name = "DL_Dossier-Echanges_RX"; scope = "DomainLocal"},
	@{name = "DL_Dossier-Administration_RX"; scope = "DomainLocal"},
	@{name = "DL_Dossier-DIRECTION_RWX"; scope = "DomainLocal"},
	@{name = "DL_Dossier-Accueil_RWX"; scope = "DomainLocal"},
	@{name = "DL_Dossier-Compta_RWX"; scope = "DomainLocal"},
	@{name = "DL_Dossier-Repro_RWX"; scope = "DomainLocal"},
	@{name = "DL_Dossier-Comm_RWX"; scope = "DomainLocal"}
)

foreach ($group in $groups) {
	"Adding group " + $group.name
	New-ADGroup -Name $group.name -Path $path -GroupScope $group.scope
}

## Structure des groupes
# DL_Dossier-Echanges_RX
	# G_TEST-FORMATION

# DL_Dossier-Administration_RX
	# DL_Dossier-DIRECTION_RWX -Members 
		# G_Direction
			# Jake Directeur
	# DL_Dossier-Accueil_RWX
		# G_Accueil
		# Leila Accueil
	# DL_Dossier-Compta_RWX
		# G_Compta
			# Mourad Comptable
	# DL_Dossier-Repro_RWX
		# G_Repro
			# Pierre Reprographie
	# DL_Dossier-Comm_RWX
		# G_Comm
			# Ali Communication

## Configuration des groupes méthode bourrin
Add-AdGroupMember `
	-Identity "DL_Dossier-Echanges_RX" `
	-Members "G_TEST-FORMATION"

Add-AdGroupMember `
	-Identity "DL_Dossier-Administration_RX" `
	-Members `
		"DL_Dossier-DIRECTION_RWX", `
		"DL_Dossier-Accueil_RWX", `
		"DL_Dossier-Compta_RWX", `
		"DL_Dossier-Repro_RWX", `
		"DL_Dossier-Comm_RWX"

Add-AdGroupMember -Identity "DL_Dossier-DIRECTION_RWX" -Members "G_Direction"
Add-AdGroupMember -Identity "DL_Dossier-Accueil_RWX" -Members "G_Accueil"
Add-AdGroupMember -Identity "DL_Dossier-Compta_RWX" -Members "G_Compta"
Add-AdGroupMember -Identity "DL_Dossier-Repro_RWX" -Members "G_Repro"
Add-AdGroupMember -Identity "DL_Dossier-Comm_RWX" -Members "G_Comm"

Add-AdGroupMember -Identity "G_Direction" -Members "jdirecteur"
Add-AdGroupMember -Identity "G_Accueil" -Members "laccueil"
Add-AdGroupMember -Identity "G_Compta" -Members "mcomptable"
Add-AdGroupMember -Identity "G_Repro" -Members "preprographie"
Add-AdGroupMember -Identity "G_Comm" -Members "acommunication"

